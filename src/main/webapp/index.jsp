<%-- 
    Document   : index
    Created on : 30-04-2021, 0:36:39
    Author     : Fabian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso de personal </title>
    </head>
    <body>
        <h1>Bienvenido a Ingresos de Personal</h1>
        <br><!-- salto de linea -->
        <h3>Rellene el Formulario.</h3>




        <form name="form" action="IngresoController" method="Post">
            Ingrese su Nombre:
            <input type="text" name="nombre" id="nombre" ><!-- ingreso nombre -->
            <br><!-- salto de linea -->
            Ingrese su Rut:
            <input type="text" name="rut" min="6" max="10" id="rut"><!-- ingreso rut -->
            <br><!-- salto de linea -->
            Ingrese su Genero:
            <input type="text" name="genero" id="genero"><!-- ingreso de genero -->
            <br><!-- }salto de linea -->
            Ingrese su Numero de Telefono:
            <input type="text" name="telefono" min="9" maxlength="9" id="telefono"><!-- ingreso de telefono -->
            <br><!-- salto de linea -->
            Ingrese su Edad:
            <input type="text" name="edad" min="1" maxlength="100" id="edad"><!-- ingreso de edad -->
            <br><!-- salto de linea -->

            <button type="submit" name="accion" value="ingreso persona" class="btn btn-success"> ingresar</button><!-- boton para ingresar -->
            <button type="" name="accion" value="ver lista persona" class="btn btn-success"> listar </button><!-- boton para ver listar -->
            
        </form>
    </body>
</html>
