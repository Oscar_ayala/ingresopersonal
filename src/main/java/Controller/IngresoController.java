/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import com.mycompany.ingresos.dao.IngresosJpaController;
import com.mycompany.ingresos.dao.exceptions.NonexistentEntityException;
import com.mycompany.ingresos.entity.Ingresos;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fabian
 */
@WebServlet(name = "IngresoController", urlPatterns = {"/IngresoController"})
public class IngresoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet IngresoController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet IngresoController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String accion = request.getParameter("accion");

        if (accion.equals("ingreso persona")) {

            try {
                String nombre = request.getParameter("nombre");
                String rut = request.getParameter("rut");
                String genero = request.getParameter("genero");
                String telefono = request.getParameter("telefono");
                String edad = request.getParameter("edad");

                Ingresos ingresos = new Ingresos(); //llamamos al Entity

                ingresos.setNombre(nombre);
                ingresos.setRut(rut);
                ingresos.setGenero(genero);
                ingresos.setTelefono(telefono);
                ingresos.setEdad(edad);

                IngresosJpaController dao = new IngresosJpaController(); //llamamos al nuestra clase dao

                dao.create(ingresos);

                processRequest(request, response);
            } catch (Exception ex) {
                Logger.getLogger(IngresoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (accion.equals("ver lista persona")) {
            IngresosJpaController dao = new IngresosJpaController(); //llamamos al nuestra clase dao
            List<Ingresos> lista = dao.findIngresosEntities();

            request.setAttribute("Lista de Ingreso", lista);
            request.getRequestDispatcher("listadoRegistro.jsp").forward(request, response);

        }

        if (accion.equals("eliminar")) {
            try {
                String rut = request.getParameter("selection");
                IngresosJpaController dao = new IngresosJpaController();
                dao.destroy(rut);

            } catch (NonexistentEntityException ex) {
                Logger.getLogger(IngresoController.class.getName()).log(Level.SEVERE, null, ex);
            }
            processRequest(request, response);
        }
        if (accion.equals("consultar")) {
            String rut = request.getParameter("selection");
            IngresosJpaController dao = new IngresosJpaController();
            Ingresos consultar = dao.findIngresos(rut);
            request.setAttribute("solicitud", consultar);
            request.getRequestDispatcher("editar.jsp").forward(request, response);
            

        }processRequest(request, response);

        if (accion.equals("editar")) {

            try {
                String nombre = request.getParameter("nombre");
                String rut = request.getParameter("rut");
                String genero = request.getParameter("genero");
                String telefono = request.getParameter("telefono");
                String edad = request.getParameter("edad");

                Ingresos ingresos = new Ingresos(); //llamamos al Entity

                ingresos.setNombre(nombre);
                ingresos.setRut(rut);
                ingresos.setGenero(genero);
                ingresos.setTelefono(telefono);
                ingresos.setEdad(edad);

                IngresosJpaController dao = new IngresosJpaController(); //llamamos al nuestra clase dao
                Ingresos solicitud = dao.findIngresos(rut);
                dao.edit(ingresos);
                List<Ingresos> lista = dao.findIngresosEntities();

                request.setAttribute("solicitud", lista);
                request.getRequestDispatcher("editar.jsp").forward(request, response);
                
               

                
            } catch (Exception ex) {
                Logger.getLogger(IngresoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
